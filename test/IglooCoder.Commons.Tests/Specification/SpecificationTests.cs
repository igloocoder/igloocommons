using System;
using IglooCoder.Commons.Specification;
using NUnit.Framework;

namespace IglooCoder.Commons.Tests.Specification
{
    public class SpecificationTests
    {
        [TestFixture]
        public class When_combining_two_specifications_using_and
        {
            [Test]
            public void both_specifications_must_return_true_for_overall_evaluation_to_return_true()
            {
                Assert.That(new TrueHelperSpecification().And(new TrueHelperSpecification()).IsSatisfiedBy("blah"), Is.True);
            }

            [Test]
            public void will_return_false_when_one_of_the_specifications_does()
            {
                Assert.That(new TrueHelperSpecification().And(new FalseHelperSpecification()).IsSatisfiedBy("blah"),
                            Is.False);
            }
        }
    }

    [TestFixture]
    public class When_negating_a_specification
    {
        [Test]
        public void should_return_true_when_specification_is_false()
        {
            Assert.That(new FalseHelperSpecification().Not().IsSatisfiedBy("asdf"), Is.True);
        }

        [Test]
        public void should_return_true_when_specification_is_true()
        {
            Assert.That(new TrueHelperSpecification().Not().IsSatisfiedBy("asdf"), Is.False);
        }
    }

    [TestFixture]
    public class When_combining_two_specifications_using_or
    {
        [Test]
        public void should_return_false_when_both_specifications_are_false()
        {
            Assert.That(new FalseHelperSpecification().Or(new FalseHelperSpecification()).IsSatisfiedBy("asdf"), Is.False);
        }

        [Test]
        public void should_return_true_when_one_of_the_specifications_is_true()
        {
            Assert.That(new FalseHelperSpecification()
                .Or(new TrueHelperSpecification())
                .IsSatisfiedBy("asdf"), Is.True);
        }
    }

    public class FalseHelperSpecification : CompositeSpecification<string>
    {
        public override bool IsSatisfiedBy(string itemToVerify)
        {
            return false;
        }
    }

    public class TrueHelperSpecification : CompositeSpecification<string>
    {
        public override bool IsSatisfiedBy(string itemToVerify)
        {
            return true;
        }
    }
}