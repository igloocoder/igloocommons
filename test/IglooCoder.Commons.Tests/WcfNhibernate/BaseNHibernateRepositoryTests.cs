using System;
using System.Collections.Generic;
using IglooCoder.Commons.WcfNhibernate;
using NHibernate;
using NUnit.Framework;
using Rhino.Mocks;

namespace IglooCoder.Commons.Tests.WcfNhibernate
{
    [TestFixture]
    public class BaseNHibernateRepositoryTests
    {
        private HelperRepository _repository;
        private ISession _session;

        [SetUp]
        public void Setup()
        {
            _session = MockRepository.GenerateMock<ISession>();

            var sessionStorage = MockRepository.GenerateStub<ISessionStorage>();
            sessionStorage.Stub(x => x.GetSession()).Return(_session);
            
            var criteria = MockRepository.GenerateStub<ICriteria>();
            criteria.Stub(x => x.List<HelperClass>()).Return(new List<HelperClass>());
            
            _session.Stub(x => x.CreateCriteria(Arg<Type>.Is.Anything)).Return(criteria);

            _repository = new HelperRepository(sessionStorage);
        }
        
        [Test]
        public void Should_use_underlying_session_to_save_or_update()
        {
            _repository.SaveOrUpdate(new HelperClass());
            _session.AssertWasCalled(x=>x.SaveOrUpdate(Arg<object>.Is.Anything));
        }

        [Test]
        public void Should_use_underlying_session_to_fetch_all()
        {
            _repository.FetchAll();
            _session.AssertWasCalled(x=>x.CreateCriteria(Arg<Type>.Is.Anything));
        }

        [Test]
        public void Should_use_underlying_session_to_fetch_by_id()
        {
            _repository.FetchById(1);
            _session.AssertWasCalled(x=>x.Get<HelperClass>(Arg<object>.Is.Anything));
        }
    }

    public class HelperRepository :BaseNHibernateRepository<HelperClass>
    {
        public HelperRepository(ISessionStorage sessionStorage) : base(sessionStorage)
        {
        }
    }

    public class HelperClass:IDomain
    {
        public long Id{ get; set;}
    }
}