﻿using IglooCoder.Commons.Eventing;

namespace IglooCoder.Commons.Tests.Eventing
{
    public class BaseHandler:IHandleMessagesOfType<PolymorphicBaseTestMessage>
    {
        public void Handle(PolymorphicBaseTestMessage message)
        {
            MessageHandled = true;
        }

        public bool MessageHandled { get; set; }
    }
}