﻿using IglooCoder.Commons.Eventing;

namespace IglooCoder.Commons.Tests.Eventing
{
    public class EventingHelperClass : IHandleMessagesOfType<TestingMessage>
    {
        public void Handle(TestingMessage message)
        {
            EventWasHandled = true;
        }

        public bool EventWasHandled { get; private set; }
    }
}