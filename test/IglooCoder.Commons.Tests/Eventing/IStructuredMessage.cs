﻿namespace IglooCoder.Commons.Tests.Eventing
{
    public interface IStructuredMessage
    {
        string Property1 { get; set; }
        int Property2 { get; set; }
    }
}