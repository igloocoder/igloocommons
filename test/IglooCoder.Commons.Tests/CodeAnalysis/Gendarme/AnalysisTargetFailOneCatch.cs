﻿using System;

namespace IglooCoder.Commons.Tests.CodeAnalysis.Gendarme
{
    public class AnalysisTargetFailOneCatch
    {

        public void DoSomethingWrong()
        {
            try
            {
                var i = 1;
            }
            catch (Exception)
            {
                var x = 1;
            }
        }     
    }
}