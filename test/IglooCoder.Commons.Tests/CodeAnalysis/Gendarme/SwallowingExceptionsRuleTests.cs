﻿using System.Linq;
using Gendarme.Framework;
using IglooCoder.Commons.CodeAnalysis.Gendarme;
using Mono.Cecil;
using NUnit.Framework;

namespace IglooCoder.Commons.Tests.CodeAnalysis.Gendarme
{
    public class SwallowingExceptionsRuleTests
    {
        [TestFixture]
        public class When_reviewing_the_AnalysisTargetPass_type
        {

            [Test]
            public void all_the_methods_should_pass()
            {
                var moduleDefinition = ModuleDefinition.ReadModule(System.Reflection.Assembly.GetAssembly(GetType()).Location);
                var type =
                    moduleDefinition.Types.FirstOrDefault(x => string.Compare(x.Name, typeof(AnalysisTargetPass).Name) == 0);

                var rule = new SwallowingExceptionsRule();

                foreach (var methodDefinition in type.Methods.Where(x=>string.Compare(x.Name, "DoSomethingProper")==0))
                {
                    var checkMethod = rule.CheckMethod(methodDefinition);
                    Assert.That(checkMethod, Is.EqualTo(RuleResult.Success));
                }
            }
        }

        [TestFixture]
        public class When_reviewing_the_AnalysisTargetFailMultiCatch_type
        {

            [Test]
            public void all_the_methods_should_fail()
            {
                var moduleDefinition = ModuleDefinition.ReadModule(System.Reflection.Assembly.GetAssembly(GetType()).Location);
                var type =
                    moduleDefinition.Types.FirstOrDefault(x => string.Compare(x.Name, typeof(AnalysisTargetFailMultiCatch).Name) == 0);

                var rule = new SwallowingExceptionsRule();

                foreach (var methodDefinition in type.Methods.Where(x=>string.Compare(x.Name, "DoSomethingWrongTwice")==0))
                {
                    var checkMethod = rule.CheckMethod(methodDefinition);
                    Assert.That(checkMethod, Is.EqualTo(RuleResult.Failure));
                }
            }
        }

        [TestFixture]
        public class When_reviewing_the_AnalysisTargetFailOneCatch_type
        {

            [Test]
            public void all_the_methods_should_fail()
            {
                var moduleDefinition = ModuleDefinition.ReadModule(System.Reflection.Assembly.GetAssembly(GetType()).Location);
                var type =
                    moduleDefinition.Types.FirstOrDefault(x => string.Compare(x.Name, typeof(AnalysisTargetFailOneCatch).Name) == 0);

                var rule = new SwallowingExceptionsRule();

                foreach (var methodDefinition in type.Methods.Where(x=>string.Compare(x.Name, "DoSomethingWrong")==0))
                {
                    var checkMethod = rule.CheckMethod(methodDefinition);
                    Assert.That(checkMethod, Is.EqualTo(RuleResult.Failure));
                }
            }
        }
    }
}