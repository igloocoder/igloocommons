using System;
using IglooCoder.Commons.Logging;

namespace IglooCoder.Commons.Tests.Logging
{
    public class HelperContext : ILoggingContext
    {
        public void Error(string messageToLog, Exception underlyingErrorException)
        {
            throw new System.NotImplementedException();
        }

        public void Debug(string messageToLog)
        {
            throw new System.NotImplementedException();
        }

        public void Info(string messageToLog)
        {
            throw new System.NotImplementedException();
        }
    }
}