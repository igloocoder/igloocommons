using IglooCoder.Commons.AOP;
using NUnit.Framework;

namespace IglooCoder.Commons.Tests.AOP
{
    public class LoggingParseTypeValidatorTests
    {
        [TestFixture]
        public class When_checking_if_a_type_should_be_valid
        {
            private ILoggingParseTypeValidator _validator;

            [SetUp]
            public void in_the_circumstances_of()
            {
                _validator = new LoggingParseTypeValidator("IglooCoder.Commons");
            }

            [Test]
            public void should_pass_when_type_name_starts_with_provided_value()
            {
                Assert.That(_validator.ShouldParseInDetail(GetType()), Is.True);
            }
        }
    }
}