using System;
using System.Collections.Generic;
using IglooCoder.Commons.AOP;
using NUnit.Framework;
using Rhino.Mocks;

namespace IglooCoder.Commons.Tests.AOP
{
    public class LoggingParameterParseTests
    {
        [TestFixture]
        public class When_parsing_parameter_array
        {
            private ILoggingParseTypeValidator _validatorMock;

            [SetUp]
            public void in_the_circumstances_of()
            {
                _validatorMock = MockRepository.GenerateMock<ILoggingParseTypeValidator>();
                _validatorMock.Stub(x => x.ShouldParseInDetail(Arg<Type>.Is.Anything)).Return(false);
                var loggingParameterParse = new LoggingParameterParse(_validatorMock);
                var parameters = new object[] {"asdf"};
                loggingParameterParse.On(parameters);
            }

            [Test]
            public void should_check_type_to_see_if_it_should_be_logged_in_detail()
            {
                _validatorMock.AssertWasCalled(x => x.ShouldParseInDetail(Arg<Type>.Is.Anything));
            }
        }

        [TestFixture]
        public class When_parsing_parameter_array_containing_an_enumerable_type
        {
            private IParseInputParameters _loggingParameterParse;
            private object[] _parameters;

            [SetUp]
            public void in_the_circumstances_of()
            {
                _parameters = new object[] {new List<string> {"asdf", "jkl;"}};
                _loggingParameterParse = new LoggingParameterParse(new LoggingParseTypeValidator("IglooCoder"));
            }

            [Test]
            public void all_items_in_the_enumerable_should_be_logged()
            {
                Assert.That(
                    _loggingParameterParse.On(_parameters),
                    Is.EqualTo("Enumerable{asdf, jkl;}"));
            }
        }

        [TestFixture]
        public class When_parsing_parameter_array_containing_complex_types
        {
            private IParseInputParameters _loggingParameterParse;
            private object[] _complexParameters;
            private ILoggingParseTypeValidator _validatorMock;
            private object[] _deepComplexParameters;

            [SetUp]
            public void in_the_circumstances_of()
            {
                _validatorMock = MockRepository.GenerateStub<ILoggingParseTypeValidator>();
                _validatorMock.Stub(x => x.ShouldParseInDetail(Arg<Type>.Is.Anything)).Return(true);

                _loggingParameterParse = new LoggingParameterParse(new LoggingParseTypeValidator("IglooCoder"));
                _complexParameters = new object[] {new ParsingHelperClass {Id = 1}};
                _deepComplexParameters = new object[]
                                             {
                                                 new DeepParsingHelperClass
                                                     {
                                                         Name = "asdfasdf",
                                                         ParsingHelper = new ParsingHelperClass {Id = 999}
                                                     }
                                             };
            }

            [Test]
            public void will_log_multiple_layers_of_complex_type()
            {
                Assert.That(
                    _loggingParameterParse.On(_deepComplexParameters),
                    Is.EqualTo(
                        "IglooCoder.Commons.Tests.AOP.DeepParsingHelperClass{asdfasdf, IglooCoder.Commons.Tests.AOP.ParsingHelperClass{999}}"));
            }

            [Test]
            public void they_are_logged_containing_the_details_of_the_object()
            {
                Assert.That(
                    _loggingParameterParse.On(_complexParameters),
                    Is.EqualTo("IglooCoder.Commons.Tests.AOP.ParsingHelperClass{1}"));
            }

            [Test]
            public void they_are_logged_starting_with_the_type_name()
            {
                Assert.That(
                    _loggingParameterParse.On(_complexParameters)
                        .StartsWith("IglooCoder.Commons.Tests.AOP.ParsingHelperClass{"),
                    Is.True);
            }
        }

        [TestFixture]
        public class When_parsing_parameter_array_containing_primitive_types
        {
            private IParseInputParameters _loggingParameterParse;
            private object[] _primitiveParameters;
            private ILoggingParseTypeValidator _validatorMock;

            [SetUp]
            public void in_the_circumstances_of()
            {
                _validatorMock = MockRepository.GenerateStub<ILoggingParseTypeValidator>();
                _validatorMock.Stub(x => x.ShouldParseInDetail(Arg<Type>.Is.Anything)).Return(false);

                _loggingParameterParse = new LoggingParameterParse(_validatorMock);
                _primitiveParameters = new object[] {1, "asdfasdf"};
            }

            [Test]
            public void they_are_logged_with_a_value_only()
            {
                    Assert.That(_loggingParameterParse.On(_primitiveParameters),
                                Is.EqualTo("1, asdfasdf"));
            }
        }
    }

    public class DeepParsingHelperClass
    {
        public string Name { get; set; }

        public ParsingHelperClass ParsingHelper { get; set; }
    }

    public class ParsingHelperClass
    {
        public int Id { get; set; }
    }
}