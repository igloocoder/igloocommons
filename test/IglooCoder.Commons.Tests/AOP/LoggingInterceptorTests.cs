using Castle.DynamicProxy;
using IglooCoder.Commons.AOP;
using NUnit.Framework;
using Rhino.Mocks;

namespace IglooCoder.Commons.Tests.AOP
{
    public class LoggingInterceptorTests
    {
        [TestFixture]
        public class When_intercepting_a_call
        {
            private MockRepository _mockery;
            private IParseInputParameters _parameterParseMock;
            private LoggingInterceptor _loggingInterceptor;
            private IInvocation _invocationMock;

            [SetUp]
            public void in_the_circumstances_of()
            {
                _mockery = new MockRepository();

                _parameterParseMock = _mockery.DynamicMock<IParseInputParameters>();
                _invocationMock = _mockery.DynamicMock<IInvocation>();
                _loggingInterceptor = new LoggingInterceptor(_parameterParseMock);

                SetupResult.For(_invocationMock.Arguments).Return(new object[1]);
            }

            [Test]
            public void the_original_call_should_proceed()
            {
                using (_mockery.Record())
                {
                    Expect.Call(() => _invocationMock.Proceed()).IgnoreArguments().Repeat.Once();
                }
                using (_mockery.Playback())
                {
                    _loggingInterceptor.Intercept(_invocationMock);
                }
            }

            [Test]
            public void the_input_parameters_and_result_should_be_parsed_for_logging()
            {
                using (_mockery.Record())
                {
                    Expect.Call(_parameterParseMock.On(new object[1])).IgnoreArguments().Return(string.Empty).Repeat.
                        Twice();
                }
                using (_mockery.Playback())
                {
                    _loggingInterceptor.Intercept(_invocationMock);
                }
            }
        }
    }
}