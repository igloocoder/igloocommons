﻿using System;

namespace IglooCoder.Commons.Eventing
{
    public interface IEventAggregator
    {
        void Register(object itemToRegister);
        void Raise<T>();
        void Raise<T>(T message);
        void Raise<T>(Action<T> action);
        void Unregister(object itemToUnregister);
    }
}