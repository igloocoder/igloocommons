﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Threading;
using Castle.DynamicProxy;

namespace IglooCoder.Commons.Eventing
{
    public class EventAggregator : IEventAggregator
    {
        private readonly IList<WeakReference> _registeredItems = new List<WeakReference>();
        private readonly SynchronizationContext _synchronizationContext;
        private readonly object _locker = new object();

        public EventAggregator()
        {
            if (SynchronizationContext.Current == null)
            {
                SynchronizationContext.SetSynchronizationContext(new DispatcherSynchronizationContext());
            }
            _synchronizationContext = SynchronizationContext.Current;
        }

        private void WhileLocked(Action action)
        {
            lock (_locker)
            {
                action();
            }
        }

        private IEnumerable<WeakReference> AllRegisteredItems
        {
            get
            {
                lock (_locker)
                {
                    return _registeredItems;
                }
            }
        }

        private void SendAction(Action action)
        {
            _synchronizationContext.Send(state => action(), null);
        }

        public void Register(object itemToRegister)
        {
            WhileLocked(() =>
            {

                if (_registeredItems.Where(x => x.Target == itemToRegister)
                        .Count() == 0 
                    && itemToRegister.GetType()
                        .GetMethods().Count() != 0)
                {
                    _registeredItems.Add(new WeakReference(itemToRegister));
                }
            });
        }

        public void Raise<T>()
        {
            Raise<T>(x => { });
        }

        public void Raise<T>(Action<T> action)
        {
            var message = CreateProxy<T>();

            action(message);

            Raise(message);
        }

        private T CreateProxy<T>()
        {
            return typeof (T).IsInterface
                ? (T) new ProxyGenerator().
                     CreateInterfaceProxyWithoutTarget(
                        typeof (T), 
                        new PropertyInterceptor())
                : (T) new ProxyGenerator().
                    CreateClassProxy(typeof (T), 
                        new PropertyInterceptor());
        }

        public void Raise<T>(T message)
        {
            SendAction(() =>
            {
                foreach (var registeredItem in AllRegisteredItems)
                {
                                   
                    var methods = from method 
                        in registeredItem.Target.GetType().GetMethods()
                        where method.GetParameters().Count() == 1 &&
                        method.GetParameters()[0]
                            .ParameterType.IsAssignableFrom(
                             message.GetType()) &&
                        method.ReturnType == typeof (void) &&
                        string.Compare(method.Name,
                            typeof (IHandleMessagesOfType<string>)
                            .GetMethods()[0].Name, true) == 0
                                    select method;

                    foreach (var methodInfo in methods)
                    {
                        try
                        {
                            methodInfo.Invoke(
                                registeredItem.Target, 
                                new object[] {message});
                        }
                        catch (Exception e)
                        {
                            //log?
                            //expose a hook to allow user to do what they want?
                        }
                    }
                }
            });
        }
    
        public void Unregister(object itemToUnregister)
        {
            WhileLocked(() =>
            {
                var itemsToRemove = new List<WeakReference>();
                foreach (var matchingRegisteredItem in 
                    AllRegisteredItems.Where(x=>x.Target == itemToUnregister))
                {
                    itemsToRemove.Add(matchingRegisteredItem);
                }
                foreach (var itemToRemove in itemsToRemove)
                {
                    _registeredItems.Remove(itemToRemove);
                }
            });
        }
    }
}