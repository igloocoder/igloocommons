﻿namespace IglooCoder.Commons.Eventing
{
    public interface IHandleMessagesOfType<T>
    {
        void Handle(T message);
    }
}