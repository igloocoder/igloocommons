﻿using Gendarme.Framework;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace IglooCoder.Commons.CodeAnalysis.Gendarme
{
    public class SwallowingExceptionsRule:Rule, IMethodRule
    {
        public RuleResult CheckMethod(MethodDefinition method)
        {
            if (!method.HasBody || !method.Body.HasExceptionHandlers)
            {
                return RuleResult.Success;
            }

            var failureOccurred = false;

            foreach (var exceptionHandler in method.Body.ExceptionHandlers)
            {
                var throwFound = false;

                var currentOperation = exceptionHandler.HandlerStart;
                while (currentOperation != exceptionHandler.HandlerEnd)
                {
                    if (currentOperation.OpCode == OpCodes.Throw || currentOperation.OpCode == OpCodes.Rethrow)
                    {
                        throwFound = true;
                        break;
                    }
                    currentOperation = currentOperation.Next;
                }
                if (!throwFound)
                {
                    failureOccurred = true;
                    if (Runner != null)
                    {
                        Runner.Report(method,Severity.High, Confidence.High, string.Format("{0} is swallowing exceptions of type {1}", method.FullName, exceptionHandler.CatchType.FullName));
                    }    
                }
            }
            
            return !failureOccurred ? RuleResult.Success : RuleResult.Failure;
        }
    }
}