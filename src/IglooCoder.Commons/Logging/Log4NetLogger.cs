using System.IO;

namespace IglooCoder.Commons.Logging
{
    public class Log4NetLogger : ILogger
    {
        private ILoggingContext _loggingContext;

        public Log4NetLogger():this("log4net.config")
        {
            
        }

        public Log4NetLogger(string configurationFileName)
        {
            log4net.Config.XmlConfigurator.Configure(new FileInfo(configurationFileName));
            _loggingContext = new Log4NetContext(log4net.LogManager.GetLogger(GetType()));
        }

        public Log4NetLogger(ILoggingContext log4netContext)
        {
            _loggingContext = log4netContext;
        }

        public ILoggingContext ContextFor(object objectToLogFor)
        {
            return _loggingContext;   
        }
    }
}