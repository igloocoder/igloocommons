using System;

namespace IglooCoder.Commons.Logging
{
    public interface ILoggingContext
    {
        void Error(string messageToLog, Exception underlyingErrorException);
        void Debug(string messageToLog);
        void Info(string messageToLog);
    }
}