namespace IglooCoder.Commons.Logging
{
    public class Log
    {
        private static ILogger _logger = new NullLogger();

        public static void Initialize(ILogger logger)
        {
            _logger = logger;
        }

        public static ILoggingContext For(object objectToLogFor)
        {
            return _logger.ContextFor(objectToLogFor);
        }
    }
}