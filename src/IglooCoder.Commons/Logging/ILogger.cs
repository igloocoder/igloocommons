namespace IglooCoder.Commons.Logging
{
    public interface ILogger
    {
        ILoggingContext ContextFor(object objectToLogFor);
    }
}