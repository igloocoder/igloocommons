namespace IglooCoder.Commons.Specification
{
    public class NotSpecification<T> : CompositeSpecification<T>
    {
        private readonly ISpecification<T> _specification;

        public NotSpecification(ISpecification<T> specification)
        {
            _specification = specification;
        }

        public override bool IsSatisfiedBy(T itemToVerify)
        {
            return !_specification.IsSatisfiedBy(itemToVerify);
        }
    }
}