using System.ServiceModel;

namespace IglooCoder.Commons.WcfNhibernate
{
    public class NHibernateContext
    {
        public static NHibernateContextExtension Current()
        {
            
            return OperationContext.Current.
                    InstanceContext.Extensions.
                    Find<NHibernateContextExtension>();
        }
    }
}