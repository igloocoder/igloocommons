using NHibernate;

namespace IglooCoder.Commons.WcfNhibernate
{
    public interface ISessionStorage
    {
        ISession GetSession();
    }
}