using System;
using NHibernate;

namespace IglooCoder.Commons.WcfNhibernate
{
    public interface INHibernateContextExtension
    {
        ISession Session { get; }
//        void InstanceContextFaulted(object sender, EventArgs e);
    }
}