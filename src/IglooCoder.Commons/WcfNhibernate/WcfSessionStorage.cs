using NHibernate;

namespace IglooCoder.Commons.WcfNhibernate
{
    public class WcfSessionStorage:ISessionStorage
    {
        public ISession GetSession()
        {
            return NHibernateContext.Current().Session;
        }
    }
}