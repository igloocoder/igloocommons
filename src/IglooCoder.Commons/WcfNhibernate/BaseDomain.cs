namespace IglooCoder.Commons.WcfNhibernate
{
    public class BaseDomain : IDomain
    {
        public virtual long Id { get; set; }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return obj.GetType() == this.GetType() && base.Equals(obj);
        }
    }
}