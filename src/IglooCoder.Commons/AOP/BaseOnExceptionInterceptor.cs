using Castle.DynamicProxy;

namespace IglooCoder.Commons.AOP
{
    public abstract class BaseOnExceptionInterceptor : BaseInterceptor
    {
        public override void OnEnter(IInvocation invocation)
        {
            //do nothing
        }

        public override void OnExit(IInvocation invocation)
        {
            //do nothing
        }

        public override void OnSuccess(IInvocation invocation)
        {
            //do nothing
        }
    }
}