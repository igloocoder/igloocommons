using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IglooCoder.Commons.AOP
{
    public class LoggingParameterParse : IParseInputParameters
    {
        private readonly ILoggingParseTypeValidator _loggingParseTypeValidator;
        private const string SEPARATOR = ", ";

        public LoggingParameterParse(ILoggingParseTypeValidator loggingParseTypeValidator)
        {
            _loggingParseTypeValidator = loggingParseTypeValidator;
        }

        public string On(object[] parametersToBeParsed)
        {
            if (parametersToBeParsed == null) return string.Empty;

            var parsedParameters = new StringBuilder();

            foreach (var parameter in parametersToBeParsed)
            {
                if (parameter == null) continue;

                if (_loggingParseTypeValidator.ShouldParseInDetail(parameter.GetType()))
                {
                    parsedParameters
                        .Append(parameter.ToString())
                        .Append("{");

                    parsedParameters.Append(On(CreateParameterArray(parameter)));

                    parsedParameters
                        .Append("}")
                        .Append(SEPARATOR);
                }
                else if (parameter is IList<string>)
                {
                    parsedParameters.Append("Enumerable{");

                    parsedParameters.Append(On((parameter as IList<string>).ToArray()));

                    parsedParameters.Append("}").Append(SEPARATOR);
                }
                else
                {
                    parsedParameters
                        .Append(parameter.ToString())
                        .Append(SEPARATOR);
                }
            }

            if (parsedParameters.Length >= SEPARATOR.Length)
            {
                parsedParameters.Remove(parsedParameters.Length - SEPARATOR.Length, SEPARATOR.Length);
            }

            return parsedParameters.ToString();
        }

        public object[] CreateParameterArray(object parameter)
        {
            var propertiesOfTheParameter = new List<object>();

            foreach (var property in parameter.GetType().GetProperties())
            {
                propertiesOfTheParameter.Add(property.GetValue(parameter, null));
            }

            return propertiesOfTheParameter.ToArray();
        }
    }
}