using System;
using Castle.DynamicProxy;

namespace IglooCoder.Commons.AOP
{
    public abstract class BaseInterceptor : IInterceptor
    {
        public void Intercept(IInvocation invocation)
        {
            OnEnter(invocation);
            try
            {
                invocation.Proceed();
                OnSuccess(invocation);
            }
            catch (Exception e)
            {
                OnException(invocation, e);
                throw;
            }
            finally
            {
                OnExit(invocation);
            }
        }

        public abstract void OnEnter(IInvocation invocation);
        public abstract void OnSuccess(IInvocation invocation);
        public abstract void OnException(IInvocation invocation, Exception exception);
        public abstract void OnExit(IInvocation invocation);
    }
}