using System;

namespace IglooCoder.Commons.AOP
{
    public class LoggingParseTypeValidator : ILoggingParseTypeValidator
    {
        private readonly string _typeNamespace;

        public LoggingParseTypeValidator(string typeNamespace)
        {
            _typeNamespace = typeNamespace;
        }

        public bool ShouldParseInDetail(Type typeBeingParsed)
        {
            return typeBeingParsed.ToString().StartsWith(_typeNamespace);
        }
    }
}